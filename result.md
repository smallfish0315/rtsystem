###1.在没有加压力情况下的测试结果

打了RT补丁的内核随着循环次数的增加 最大响应时间变化比较小，而未打RT补丁的内核随着循环次数的增加最大响应时间也逐渐增加，具体测试数据如下：

+ 循环1000次
```
./cyclictest -t -p 99 -q -n -l 1000 > 1000/1
```

 + 打了RT补丁的测试结果
```
root@ok335x:~/1000# cat  1 2 3 4 5 6 7
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   13 Avg:   13 Max:      40
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   10 Avg:   13 Max:      33
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:     10 Act:   13 Avg:   13 Max:      78
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   14 Avg:   13 Max:      46
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   14 Avg:   13 Max:      37
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:     10 Act:   13 Avg:   13 Max:      46
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   14 Avg:   13 Max:      38
```

 + 未RT补丁的测试结果
```
smallfish@F0315:~/work/urt/1000$ cat 1 2 3 4 5 6 7
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   12 Avg:   11 Max:     171
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   12 Avg:   11 Max:     396
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   12 Avg:   11 Max:     335
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   12 Avg:   11 Max:     325
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   12 Avg:   11 Max:     385
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   12 Avg:   11 Max:     112
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   1000 Min:      9 Act:   12 Avg:   11 Max:     382
```

+ 循环5000次
```
./cyclictest -t -p 99 -q -n -l 5000 > 5000/1
```

 + 打了RT补丁的测试结果
```
root@ok335x:~/5000# cat 1 2 3 4 5 6 7
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:      9 Act:   15 Avg:   13 Max:      48
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:     10 Act:   16 Avg:   14 Max:      42
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:     10 Act:   15 Avg:   14 Max:      39
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:     10 Act:   15 Avg:   14 Max:      53
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:     10 Act:   13 Avg:   14 Max:      30
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:     10 Act:   16 Avg:   14 Max:      43
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:      8 Act:   32 Avg:   13 Max:      49
```

 + 未打RT补丁的测试结果
```
smallfish@F0315:~/work/urt/5000$ cat 1 2 3 4 5 6 7
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:      8 Act:   10 Avg:   11 Max:     303
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:      9 Act:   13 Avg:   13 Max:     720
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:      9 Act:   13 Avg:   12 Max:     378
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:      9 Act:   13 Avg:   12 Max:     850
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:      9 Act:   13 Avg:   13 Max:     229
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:      9 Act:   14 Avg:   14 Max:     788
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:   5000 Min:      9 Act:   12 Avg:   13 Max:     725
```

+ 循环10000次
```
./cyclictest -t -p 99 -q -n -l 10000 > 10000/1
```

 + 打了RT补丁的测试结果
```
root@ok335x:~/10000# cat 1 2 3 4 5 6 7
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:      9 Act:   15 Avg:   14 Max:      52
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     11 Act:   15 Avg:   14 Max:      42
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     11 Act:   15 Avg:   14 Max:      39
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     11 Act:   15 Avg:   15 Max:      36
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     11 Act:   14 Avg:   14 Max:      47
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     11 Act:   14 Avg:   14 Max:      42
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:      9 Act:   13 Avg:   14 Max:      47
```

 + 未打RT补丁的测试结果
```
smallfish@F0315:~/work/urt/10000$ cat 1 2 3 4 5 6 7
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:      9 Act:   13 Avg:   13 Max:    1021
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     10 Act:   14 Avg:   13 Max:     594
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:      9 Act:   14 Avg:   14 Max:    1282
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     10 Act:   13 Avg:   14 Max:     916
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     11 Act:   14 Avg:   14 Max:     622
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:      9 Act:   15 Avg:   14 Max:    1158
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:      9 Act:   14 Avg:   13 Max:     883
```


###2.加压测试结果

+ 加压力后的测试结果，能够非常明显看出打了RT补丁对响应时间的提升非常大。
+ 由于测试加压一次一次输入命令会影响测试结果，直接采用脚本一次输入(启动XXXX是循环次数、XX是组数、XXX是文件数)
```
./cyclictest -t -p 99 -q -n -l XXXX > 10000/1 &
./cyclictest -t -p 99 -q -n -l XXXX > 10000/2 &
./cyclictest -t -p 99 -q -n -l XXXX > 10000/3 &
./cyclictest -t -p 99 -q -n -l XXXX > 10000/4 &
./cyclictest -t -p 99 -q -n -l XXXX > 10000/5 &
./cyclictest -t -p 99 -q -n -l XXXX > 10000/6 &
./cyclictest -t -p 99 -q -n -l XXXX > 10000/7 &
./backbench -g XX -f XXX &
```

#####1）打了RT补丁的情况

+ Running in process mode with 40 groups using 40 file descriptors each (== 1600 tasks) Each sender will pass 100 messages of 100 bytes
```
root@ok335x:~/stress# cat 1 2 3 4 5 6 7
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     15 Act:   21 Avg:   27 Max:      52
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     12 Act:   26 Avg:   25 Max:      45
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     12 Act:   27 Avg:   23 Max:      40
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     12 Act:   28 Avg:   24 Max:      71
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     11 Act:   28 Avg:   21 Max:      47
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     20 Act:   46 Avg:   39 Max:      71
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     14 Act:   36 Avg:   27 Max:      46
```

+ Running in process mode with 50 groups using 160 file descriptors each (== 8000 tasks) Each sender will pass 100 messages of 100 bytes
```
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     12 Act:   35 Avg:   20 Max:    2551
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     13 Act:   15 Avg:   21 Max:    2531
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     12 Act:   17 Avg:   21 Max:    2475
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     13 Act:   27 Avg:   27 Max:    2436
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     13 Act:   26 Avg:   20 Max:    2462
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     11 Act:   43 Avg:   25 Max:    2455
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     13 Act:   20 Avg:   28 Max:    2551
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     15 Act:   18 Avg:   30 Max:    2463
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     12 Act:   18 Avg:   21 Max:    2405
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     13 Act:   31 Avg:   24 Max:    2476
```

#####2）未打RT补丁的情况
+ Running in process mode with 40 groups using 40 file descriptors each (== 1600 tasks) Each sender will pass 100 messages of 100 bytes
```
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     13 Act:11899863 Avg:5793648 Max:12070340
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     14 Act:11901058 Avg:5791115 Max:12070483
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     18 Act:11899914 Avg:5795724 Max:12071080
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     12 Act:11899637 Avg:5796287 Max:12070330
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     14 Act:11899955 Avg:5795222 Max:12070299
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     14 Act:11884490 Avg:5826844 Max:12071065
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     13 Act:11882575 Avg:5829986 Max:12070679
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     16 Act:11884909 Avg:5825476 Max:12070572
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     14 Act:11878780 Avg:5836340 Max:12070360
# /dev/cpu_dma_latency set to 0us
T: 0 (   -1) P:99 I:1000 C:  10000 Min:     14 Act:11875644 Avg:5841749 Max:12070698
```

+ Running in process mode with 50 groups using 160 file descriptors each (== 8000 tasks) Each sender will pass 100 messages of 100 bytes
```
等了很久也没出结果
```

