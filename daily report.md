#2015.04.07

```cpp
int  clock_gettime  (clockid_t  clockid, struct timespec  *tv)
{
	return  (lib_clock_gettime  (clockid, tv) );
}
```


```cpp
int  clock_settime  (clockid_t  clockid, const struct timespec  *tv)    \
        {   \
            return  (lib_clock_settime  (clockid, tv) );   \
        }
```

+ sylinos 的




#2015.04.08

```cpp
#define CLOCK_REALTIME              0                   //系统绝对时间          /*  实时时钟源 (设置时间会影响) */
#define CLOCK_MONOTONIC             1                   //单调时间                /*  (设置时间不会影响)          */
#define CLOCK_PROCESS_CPUTIME_ID    2
#define CLOCK_THREAD_CPUTIME_ID     3
```

+ clock_getres()函数

```cpp
static inline int
realtime_getres (struct timespec *res)
{
  long int clk_tck = sysconf (_SC_CLK_TCK);   // linux下得到的clk_tck=100

  if (__glibc_likely (clk_tck != -1))
    {
      /* This implementation assumes that the realtime clock has a
         resolution higher than 1 second.  This is the case for any
         reasonable implementation.  */
      res->tv_sec = 0;
      res->tv_nsec = 1000000000 / clk_tck;
      return 0;
    }

  return -1;
}

/* Get resolution of clock.  */
int
__clock_getres (clockid_t clock_id, struct timespec *res)
{
  int retval = -1;

  switch (clock_id)
    {
#ifdef SYSDEP_GETRES
      SYSDEP_GETRES;
#endif

#ifndef HANDLED_REALTIME
    case CLOCK_REALTIME:
      retval = realtime_getres (res);
      break;
#endif  /* handled REALTIME */

    default:
#ifdef SYSDEP_GETRES_CPU
      SYSDEP_GETRES_CPU;
#endif
#if HP_TIMING_AVAIL
      if ((clock_id & ((1 << CLOCK_IDFIELD_SIZE) - 1))
          == CLOCK_THREAD_CPUTIME_ID)
        retval = hp_timing_getres (res);
      else
#endif
        __set_errno (EINVAL);
      break;

#if HP_TIMING_AVAIL && !defined HANDLED_CPUTIME
    case CLOCK_PROCESS_CPUTIME_ID:
    case CLOCK_THREAD_CPUTIME_ID:
      retval = hp_timing_getres (res);
      break;
#endif
    }

  return retval;
}

```

+ clock_gettime()函数

###高精度时钟 学习
+ 最著名的计数器寄存器就是TSC(timestamp counter，时间戳计数器) 64位寄存器，记录CPU时钟走周期数，从内核态和用户态都可以读取它。
```cpp
rdtsc(low32,high32)  // 将64位的数值读到两个32位变量中
rdtscl(low32)		 // 只读取寄存器中的低半部分
rdtscll(var64)		 // 将64位的数值读到一个long long型的变量
```
大多数常见的TSC应用中，读取计数器寄存器的低半部分就够了。

+ 测量指令自身的时间
```cpp
unsigned long begin,end;
rdtscl(begin);rdtscl(end);
end-begin;
```

+ 其他一些平台提供类似功能，与体系结构无关的函数  `get_cycles`代替`rdtsc`，它定义在`<asm/timex.h> (<linux/timex.h>)`
各种平台都可以使用这个函数，在没有时钟周期计数寄存器的平台上它总是返回0。

+ 关于SMP的情况，在SMP系统中，它们不会在多个处理器间保持同步。为了确保获得一致的值，我们需要为查询计数器的代码禁止抢占。

### 获取当前时间
+ 函数`do_gettimeofday`，用秒或微妙值来填充一个指向struct timeval的指针变量
```cpp
struct timeval
{
	time_t tv_sec; //秒 [long int]
	suseconds_t tv_usec; //微秒 [long int]
};
```

+ 当前时间也可以通过xtime变量(`struct timespec`)来获的，但精度要差一些，

+ 因为很难原子的访问`timeval`变量的两个成员。因此kernel提供了一个辅助函数`current_kernel_time`
```cpp
#include<linux/time.h>
struct timespec current_kernel_time
```
+ jit(Just in time)模块，将创建`/proc/currentime`文件

+ 64位`jiffies`计数器的高32位字的最低位被置1，这是因为`INTIAL_JIFFIES`的默认值所致。 当内核从计数器中抽取运行时，会减去该初始值。


###参考
+ `HZ` 符号指出每秒钟产生的时钟次数
+ `u64 jiffies_64` 变量会在每次时钟递增，也就是说每秒会增加HZ次。在64位平台`jiffies`和`jiffies_64`一致，在32位平台`jiffies`是`jiffies_64`的低32位



#2015.04.09
###sylinxos 时钟初始化流程

```mermaid
graph TD;
  A[系统内核入口API_KernelPrimaryStart]-->B[启动内核_KernelPrimaryEntry];
  B-->C[初始化系统时钟bspTickInit];
  C-->F[启动新线程tickThread];
  C-->D[am335xEnableModuleClock];
  C-->E[配置-初始化-打开中断-开始DMTimer];
```

1. `DMTimerEnable（）`开启DMTimer
2. `DMTimerDisable（）` 关闭DMTimer
3. `DMTimerWaitForWrite()` 等待上一次写入的完成
4. `DMTimerModeConfigure()` 清空TCLR寄存器中的AR和CE ，设置时间模式到TCLR中
5. `DMTimerCounterGet()`获取计数器的计数值 HWREG（baseAdd + DMTIMER_TCRR）

`int _G_uiFullCnt`


+ 通过`LW_CFG_TICKS_PER_SEC`来定义操作系统时钟频率
```cpp
#define LW_CFG_TIME_TICK_HOOK_EN                1       /*   是否允许操作系统时钟挂钩                   */
#define LW_CFG_TICKS_PER_SEC                    100     /*   定义操作系统时钟频率    (Hz)               */
                                                        /*   推荐 100 ~ 1000 之间                     */
#define LW_CFG_TIME_HIGH_RESOLUTION_EN          1       /*   提供获取高精度时间分辨率接口               */
```

+ baseAdd的宏定义
```cpp
/** @brief Base addresses of DMTIMER memory mapped registers                  */
#define SOC_DMTIMER_0_REGS                   (0x44E05000)
#define SOC_DMTIMER_1_REGS                   (0x44E31000)
#define SOC_DMTIMER_2_REGS                   (0x48040000)  // bspTickInit主要用到的值
#define SOC_DMTIMER_3_REGS                   (0x48042000)
#define SOC_DMTIMER_4_REGS                   (0x48044000)
#define SOC_DMTIMER_5_REGS                   (0x48046000)
#define SOC_DMTIMER_6_REGS                   (0x48048000)
#define SOC_DMTIMER_7_REGS                   (0x4804A000)
```

* * *


###sylixos clock_gettime的流程



#2015.04.10
####对sylixos分析



#### 安装gnuplot






###参考补充
```cpp
#define	REGISTER                register	                            /*  寄存器变量                  */
#ifdef __GNUC__
#define LW_INLINE               inline                                  /*  内联函数定义                */
#else
#define LW_INLINE               __inline
#endif
```












